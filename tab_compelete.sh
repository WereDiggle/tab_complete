# Attempts to tab complete any tcl scripts, even if they are in an alias.
_supportedTools()
{
    local cur
    cur=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=()
 
    # We need to figure out which clearCase directory the tcl tool is being called from.
    local tclString=${COMP_WORDS[0]}
    local clearCaseView
    if [ -n "${aliasViewMap[${tclString}]}" ] 
    then
      clearCaseView="${aliasViewMap[${tclString}]}"
    else 
      clearCaseView="${tclString%%/fwtest/bin/tcl}"
    fi
    
    # After typing "tcl" you will want to see the list of supported tools.  If COMP_CWORD 
    # is greater than 1 it means you've already chosen a tool and you'll want tool specific
    # completion
    if [ $COMP_CWORD -eq 1 ]
    then
      # Users will still want to run regular .tcl files so that should be included in the completion
      # Include directories in the working directory so they can tab complete a path to .tcl file
      compopt -o plusdirs
      
      # Include all executables in the supportedTools folder
      COMPREPLY=($(compgen -X "*.*" -W "$(for i in ${clearCaseView}/fwtest/TLA/bin/supportedTools/*; do echo $(basename $i); done)" -- $cur))
      
      # Include all .tcl files in the supportedTools folder
      COMPREPLY+=($(compgen -X "!*.tcl" -W "$(for i in ${clearCaseView}/fwtest/TLA/bin/supportedTools/*; do echo $(basename $i); done)" -- $cur))
      
      # Include all .tcl files in the working directory
      COMPREPLY+=($(compgen -f -X "!*.tcl" -- ${cur}))
    else
      _toolSpecificCompletion "$cur" "$clearCaseView"
    fi
    return 0
}

# Attempts to complete general paramaters as defined in the .args file of the tool that's being completed
# EXAMPLE: tcl tce -testG<tab> => tcl tce -testGroup 
_generalParameterCompletion()
{
  local cur tool clearCaseView
  cur=$1
  tool=${COMP_WORDS[1]}
  clearCaseView=$2
  
  argsFile="${tool}.args"
  argsLocation="${clearCaseView}/fwtest/TLA/bin/supportedTools/${argsFile}"
  
  if [ -f $argsLocation ]
  then
    while read param
      do
      paramName="${param%%:::*}"
      parameters="${parameters}${IFS}${paramName}"
    done <${argsLocation}
    COMPREPLY=($(compgen -W "$parameters" -- $cur))
  else
    COMPREPLY=($(compgen -W "-help" -- $cur))
  fi
}

# Attempts to complete general values after parameters defined in the .args file of the tool
# EXAMPLE: tcl tce -start "tom<tab>" tcl tce -start "tomorrow"
# -start is defined in tce.args as -start::: ClockScan
# there is also a ClockScan.type file that defines all the words of the specific type
# Note: This implementation of types only supports alphabetical words as numbers are often
#       variable and it's hard to tell when they are not. Other characters are usually used to 
#       break individual words up, so those are also not supported.
_generalArgumentCompletion()
{
  local param cur tool clearCaseView
  cur=$1
  param=$2
  tool=${COMP_WORDS[1]}
  clearCaseView=$3
  
  argsFile="${tool}.args"
  argsLocation="${clearCaseView}/fwtest/TLA/bin/supportedTools/${argsFile}"
  
  if [ -f $argsLocation ]
  then
  
    paramString=$(grep "^${param}\b" "${argsLocation}")
    paramTypeString="${paramString#*:::}"
    
    # Check if the param has types defined. 
    # then add the words for that type to COMPREPLY
    if [ -n "${paramTypeString}" ]
    then
      IFS=' ' read -ra paramTypes <<< "${paramTypeString}"
      
      # TODO: use COMP_LINE and COMP_POINT to find curWord
      # This variable is for when cur is actually multiple words in a quoted string, or separated by some
      # non-letter character.
      # EX: tcl tce -badExample "a b<cursor> c d"
      # we want to complete b to blarg, but leave a c and d as is. 
      # This means we need a prefix for those words that come before b, 
      # otherwise they'll just get replaced
      local curString="${COMP_LINE:0:COMP_POINT}"
      
      # We'll want to get rid of everything from start to param
      # Since we know that param is going to be in the line
      curString="${curString##*$param}"
      
      # trim leading whitespace
      curString="$(echo -e "${curString}" | sed -e 's/^[ \t]*//')"
      
      # We don't want the opening quote in the prefix if it exists
      curString="${curString##*[\"\']}"
      
      # we only want to complete the first word before the cursor
      local curWord="${curString##*[^[:alpha:]]}"
      
      # Need the prefix to allow bash completion to complete just one word.
      # tcl tce -dur "tomorrow 8h<tab>" => tcl tce -dur "tomorrow 8hour"
      # instead of
      # tcl tce -dur "tomorrow 8h<tab>" => tcl tce -dur "hour"
      local prefix="${curString%$curWord}"
      COMPREPLY=()
      for paramType in ${paramTypes[@]}
      do
        # TODO: change this to the same place as argsLocation
        local typeFile="${clearCaseView}/fwtest/TLA/bin/supportedTools/${paramType}.type"
        
        # need to change IFS temporarily so that spaces in the prefix
        # don't separate the completion into multiple completions
        local oldIFS=${IFS}
        IFS=$'\n'
        if [ -f ${typeFile} ]
        then
            # by default, don't add the space. Since they might want to add more to whatever they're writting
            # It's easier to hit the spacebar than the backspace anyways
            compopt -o nospace
            local content=$(grep "^${curWord}" ${typeFile})
            COMPREPLY+=($(compgen -W "${content}" -P "${prefix}" -- "${curWord}"))
        fi
        IFS=${oldIFS}
      done
    fi
  fi
}

_toolSpecificCompletion()
{
  local cur prev tool clearCaseView
  cur=$1
  prev=${COMP_WORDS[COMP_CWORD-1]}
  tool=${COMP_WORDS[1]}
  clearCaseView=$2
  escapedClearCaseView="${clearCaseView//\//\\\/}"
  
	if [ "$tool" == "traffic" ]
	then
    # TODO: fix this to be better, pretty sure this won't work with more than 2 double quotes
    local traffic_words="${cur%\"}"
    traffic_words="${traffic_words#\"}"
	  _tclTrafficCompletion "${traffic_words}"
  elif [ "$tool" == "tce" ]
  then
    _tclTceCompletion "$cur" "$clearCaseView"
  
  # Everything else
  else     
    argsFile="${tool}.args"
    argsLocation="${clearCaseView}/fwtest/TLA/bin/supportedTools/${argsFile}"
    helpFile="${tool}.help"
    helpLocation="/home/rzou/tab_complete/${helpFile}"
    # Each tool here follows the "name value" pair convention.  Every second
    # argument will be autocompleted - assuming there is a .args file for the tool
    # If the value is -help
    declare -A paramTypeMap
    if [ $(($COMP_CWORD % 2)) -eq 0]
    then
      _generalParameterCompletion "$cur" "$clearCaseView"
    elif [ "$prev" == "-profile" -o "$prev" == "-file" -o "$prev" == "-soakConfig" -o "$prev" == "-baseDir" ]
    then
      # We allow the default "filename" autocomplete for the above arguments
      # because users can provide filename arguments
      COMPREPLY=($(compgen -o default -- $cur))
    else 
      _generalArgumentCompletion "${cur}" "${prev}" "${clearCaseView}"
    fi
  fi
}

# this is basically the same completion except, specific to arguments in -traffic "{trafficType} ..."
_tclTrafficCompletion()
{
  local traffic_words
  IFS=' ' read -ra traffic_words <<< "$1"
  local trafficMode="${traffic_words[0]}"
  
  case "${trafficMode}" in
    rt)   trafficMode="realTraffic" ;;
    fs)   trafficMode="fullSocket" ;;
    bc)   trafficMode="bitCannon" ;;
    im)   trafficMode="internetMix" ;;
    fsim) trafficMode="fsinternetMix" ;;
    *)    ;;
  esac 
  
  local helpFile="${trafficMode}.help"
  local helpLocation="/home/rzou/tab_complete/${helpFile}"
  local argsFile="${trafficMode}.args"
  local argsLocation="${clearCaseView}/fwtest/TLA/bin/supportedTools/${argsFile}"
  
	for (( i=0 ; i<${#traffic_words[@]} ; i++ ));
	  do
	  echo -e "\n${i}: ${traffic_words[$i]}"
	done
}

_tclTceHelpCompletion()
{
  local helpLocation=$1
  local helpParameter=$2
  
  if [ -f $helpLocation ]
  then
    # COMP_TYPE holds a value that represents a state when tab completion is triggered
    # 63 means that this is not the first time tab is hit for the current line.
    # usually shows a list of multiple possible completions if first tab didn't complete anything.
    if [ ${COMP_TYPE} -eq 63 ]
      then
      answer=$(sed -n '/'"$helpParameter".*:'/,/^$/p' $helpLocation )
      echo -e "\n$answer" >&2
    fi

    # if there isn't one completion option, then "end of help" online prints after the second [tab].
    # But the echo prints on both key presses.
    COMPREPLY=("###################" "END OF HELP ######" )
  else
    # We need to play around with the IFS becuase otherwise compgen will
    # split up the "answer" below by each space
    local OLDIFS=$IFS
    local IFS=$'\n'
    answer="< Please use -help for detailed info on each argument >"
    COMPREPLY=($(compgen -W "$answer" -- $cur))
    IFS="$OLDIFS"
    unset IFS
    # This basically allows us to display the "help" message above
    # but NOT use it as an argument
    if [ ${#COMPREPLY[*]} -eq 1 ] 
    then
      COMPREPLY=()
    fi
  fi
}

_tclTceCompletion()
{
  local cur prev tool clearCaseView
  cur=$1
  prev=${COMP_WORDS[COMP_CWORD-1]}
  tool=${COMP_WORDS[1]}
  clearCaseView=$2
  escapedClearCaseView="${clearCaseView//\//\\\/}"
  
  local helpFile="${tool}.help"
  local helpLocation="/home/rzou/tab_complete/${helpFile}"
  
  # Each tool here follows the "name value" pair convention.  Every second
  # argument will be autocompleted - assuming there is a .args file for the tool
  # If the value is -help
  local parameters=""
  declare -A paramTypeMap
  if [ "${cur}" == "-h" -o "${cur}" == "-help" ]
  then
    _tclTceHelpCompletion "${helpLocation}" "${prev}"
  elif [ "${prev}" == "-h" -o "${prev}" == "-help" ]
  then
    _tclTceHelpCompletion "${helpLocation}" "${COMP_WORDS[COMP_CWORD-2]}"
  elif [ $(($COMP_CWORD % 2)) -eq 0 ]
  then
    _generalParameterCompletion "$cur" "$clearCaseView"
  else
    if [ "$prev" == "-profile" -o "$prev" == "-file" -o "$prev" == "-soakConfig" -o "$prev" == "-baseDir" ]
    then
      # We allow the default "filename" autocomplete for the above arguments
      # because users can provide filename arguments
      COMPREPLY=($(compgen -o default -- $cur))
  
    # We have a unique completion for -tests that goes into the sw-tc in the the clear case view used 
    # to run the tool. completion follows file completion rules until it runs into ICs, at which point.
    # it looks inside the tc.tcl for IC names. DOES NOT HANDLE IC regexp, unforturnately.
    elif [ "$prev" == "-tests" ]
      then

        # start and end quote will have the character that ends/starts the cur string
        # when it isn't a quoting character, it'll be empty
        local startQuote="${cur:0:1}"
        startQuote="${startQuote#[^\"\']}"
        cur="${cur#[\"\']}"
        
        local endQuote="${cur:(-1):1}"
        endQuote="${endQuote#[^\"\']}"
        cur="${cur%[\"\']}"
        
        local quote
        [ -n "$startQuote" ] && quote="$startQuote" || quote="$endQuote"

        local curDir="${clearCaseView}/sw-tc"
        local curLo="${curDir}/${cur}"
        
        # we need to know if what we're completing ends with a /
        # if it doesn't don't treat it like a directory and show it's contents yet
        local endsWithSlash
        [ "${cur: -1}" == '/' ] && endsWithSlash=true || endsWithSlash=false
        
        # we know the structure of ICs, it's ROOT/SUITE/TESTCASE/IC, take advantage of this
        local IC=""
        local atIC
        
        # We have ROOT/SUITE/TESTCASE/ but no IC, active flag to show ICs instead of files 
        [ ${#ADDR[@]} -eq 3 -a "$endsWithSlash" = true ] && atIC=true || atIC=false
        
        IFS='/' read -ra ADDR <<< "$cur"
        # if there's 4 then it's at the IC level
        # this won't matter if ROOT,SUITE,TESTCASE isn't correct
        if [ ${#ADDR[@]} -eq 4 ] 
          then 
          IC=${ADDR[3]}
          atIC=true
          unset ADDR[3]
        fi
        
        # only show completions or complete if the path is actually valid so far
        local pathIsValid
        
        # There shouldn't be a slash after an IC
        [ -n "$IC" -a "$endsWithSlash" = true ] && pathIsValid=false || pathIsValid=true
        
        # There should only be 4 levels ROOT/SUITE/TESTCASE/IC 
        [ ${#ADDR[@]} -gt 4 ] && pathIsValid=false || pathIsValid=true
        
        for i in ${ADDR[@]};
          do
          # need to account for incomplete paths cause they can be valid.
          # If this is the last one and there's no IC defined and it's not ended with a slash then we
          # don't have to verify the last dir here. We can just leave it to the next part.
          if [ "${i}" == "${ADDR[${#ADDR[@]}-1]}" -a -z "$IC" -a "$endsWithSlash" != true ]
            then
            break
          fi
          curDir="${curDir}/${i}"
          if [ ! -d "$curDir" ]
            then
            pathIsValid=false
            break
          fi
        done        
        
        if [ "$pathIsValid" = true ]
          # get the list of possibilities
          then
          local content=""
          if [ "$atIC" = true ]
            then
            
            #if the cursor is after the end quote, then move on. We are done with -tests at that point.
            if [ "${COMP_LINE:COMP_POINT-1:1}" == "$quote" ]
              then
              cur="^$cur$"
            fi
            
            content=$( find $curDir -follow -maxdepth 1 -name "*.tcl" | xargs grep -H "^ *ic *"  | tr -d "\\" | sed 's/ ic //' | sed 's/tc\.tcl//' | tr -d ": " | sed "s/${escapedClearCaseView}\/sw-tc\///" | grep "$cur")
            COMPREPLY=($(compgen -P "$quote" -S "$quote" -W "$content"))
          elif [ "$endsWithSlash" = true ]
            then
            compopt -o nospace
            
            content=$(find $curDir -maxdepth 1 -mindepth 1 \( ! -iname "*.tcl" \) | sed "s/${escapedClearCaseView}\/sw-tc\///")
            COMPREPLY=($(compgen -P "$quote" -S '/' -W "$content" -- $cur))
          # At this point, it means that $cur is an incomplete path
          else
            compopt -o nospace
            
            content=$(find $curDir -maxdepth 1 -mindepth 1 \( ! -iname "*.tcl" \) | sed "s/${escapedClearCaseView}\/sw-tc\///" | grep "^$cur")
            COMPREPLY=($(compgen -P "$quote" -S '/' -W "$content" -- $cur))
          fi
        fi 
    
    # If they haven't entered anything for the current parameter
    # then the default behaviour is to display the help page
    elif [ -z "${cur}" ]
    then
      _tclTceHelpCompletion "${helpLocation}" "${prev}"
    
    # Try to find the completion type for the parameter
    # and complete accordingly
    else
      _generalArgumentCompletion "${cur}" "${prev}" "${clearCaseView}"
    fi
  fi
}



# This only runs once every time it gets sourced. Do all the slow stuff below.

# complete for all tcl aliases.
# since this script runs in a subshell, it can't get the list of active aliases from the alias command
# So instead, check in all the usual places for aliases. If they're not active, then oh well, you don't 
# need tab completion.
echo "$USER"
echo "$HOME"
allSource=""
aliasFiles=(~/.bashrc ~/.bash_profile ~/.profile ~/.aliases ~/.bash_aliases ~/.profile_aliases)
for aliasFile in ${aliasFiles[@]};
  do
  if [ -f $aliasFile ]
    then 
    echo "Grabbing aliases from $aliasFile"
    allSource="${allSource}$(grep ^alias $aliasFile | sed 's/^alias //')"
  fi  
done 

declare -A aliasViewMap

while read -r line;
  do
  # if the line starts with alias and the actual command ends with /tcl
  # note: we are screwed if they used semicolons before an alias on the same line
  
  # Uses globbing not regexp 
  aliasName="${line%%=*}"
  aliasString="${line#*=}"
  
  # replace $HOME and $USER with right variables
  aliasString="${aliasString//\$USER/$USER}"
  aliasString="${aliasString//\$HOME/$HOME}"
  
  # remove single quotes and double quotes 
  aliasString="${aliasString//\"/}"
  aliasString="${aliasString//\'/}"
  
  # if aliasString ends with /tcl then that's good enough for tab completion
  if [[ "$aliasString" =~ "/bin/tcl" ]]
    then 
    echo "$aliasName"
    aliasViewMap[${aliasName}]="${aliasString%%/fwtest/bin/tcl}"
    complete -F _supportedTools ${aliasName}
  fi  
done <<< "$allSource"

